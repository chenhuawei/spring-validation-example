package demo.service.impl;

import demo.service.ServiceA;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 16-4-27.
 */
@Service("serviceA")
public class ServiceAImpl implements ServiceA {

    public String method() {
        System.out.println("ServiceA.method");
        return "ServiceA.method";
    }
}
