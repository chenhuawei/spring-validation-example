package demo.service.impl;

import demo.service.ServiceB;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 16-4-27.
 */

@Service("serviceB")
public class ServiceBImpl implements ServiceB {

    public String method() {
        System.out.println("ServiceB.method");
        return "ServiceB.method";
    }
}
