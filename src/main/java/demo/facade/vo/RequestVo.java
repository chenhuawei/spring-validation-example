package demo.facade.vo;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * Created by Administrator on 16-5-4.
 */
public class RequestVo {
    @Min(value = 1, message = "{int.is.error}")
    private int intValue;

    @Digits(integer = 4, fraction = 2, message = "{float.is.error}")
    private float floatValue;

    @Digits(integer = 12, fraction = 4, message = "{double.is.error}")
    private double doubleValue;

    @NotNull(message = "{string.is.error}")
    private String stringValue;

    @Past(message = "{date.is.error}")
    private Date dateValue;

    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public void setFloatValue(float floatValue) {
        this.floatValue = floatValue;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public void setDoubleValue(double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }
}
