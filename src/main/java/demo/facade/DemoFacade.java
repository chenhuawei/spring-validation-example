package demo.facade;

import demo.facade.vo.RequestVo;
import demo.service.ServiceA;
import demo.service.ServiceB;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 16-4-27.
 */
@Controller
@RequestMapping("/demo")
public class DemoFacade {
    @Resource(name = "serviceA")
    ServiceA serviceA;

    @Resource(name = "serviceB")
    ServiceB serviceB;

    @RequestMapping(value = "/serviceA", method = RequestMethod.GET)
    public ResponseEntity<String> requestServiceA() {
        String str = serviceA.method();
        return new ResponseEntity<String>(str, HttpStatus.OK);
    }

    @RequestMapping(value = "/serviceB", method = RequestMethod.GET)
    public ResponseEntity<String> requestServiceB() {
        String str = serviceB.method();
        return new ResponseEntity<String>(str, HttpStatus.OK);
    }

    @RequestMapping(value = "/valid", method = RequestMethod.POST)
    public ResponseEntity<Object> valid(@Valid @RequestBody RequestVo reqVo, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = new ArrayList<String>();
            List<ObjectError> all = result.getAllErrors();
            for (ObjectError err : all) {
                errors.add(err.getDefaultMessage());
            }
            return new ResponseEntity<Object>(errors, HttpStatus.OK);
            /*
            Map<String, String> response = new HashMap<String, String>();
            List<FieldError> list = result.getFieldErrors();
            for (FieldError err : list) {

                response.put(err.getField(), err.getDefaultMessage());
            }
            return new ResponseEntity<Object>(response, HttpStatus.OK);
            */


        }
        return new ResponseEntity<Object>(reqVo, HttpStatus.OK);
    }
}
