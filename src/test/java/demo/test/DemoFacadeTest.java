package demo.test;

import demo.facade.DemoFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.Resource;


@ContextConfiguration(locations = {
        "classpath*:application/applicationContext.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class DemoFacadeTest {

    MockMvc mockMvc;
    @Resource
    DemoFacade demoFacade;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(demoFacade).build();
    }

    @Test
    public void testServiceA() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/demo/serviceA"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("ServiceA.method"));
    }

    @Test
    public void testServiceB() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/demo/serviceB"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("ServiceB.method"));
    }
}